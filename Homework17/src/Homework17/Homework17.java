package Homework17;

import java.util.*;

public class Homework17 {

    public static void main(String[] args) {
        String inputString = "На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке. Необходимо посчитать!";
        inputString = inputString.toLowerCase();
        inputString= inputString.replace("!", " ");
        inputString= inputString.replace(",", " ");
        inputString= inputString.replace(".", " ");
        inputString = inputString.trim();
        // делим строку по пробелам
       String[] inputStringArrayAfterSplit = inputString.split(" ");
        // Создаем мап на основании массива после разбора строки
        //
        // Пробелы не учитываются!
        //
        Map<String, Integer> resultMap = new HashMap<>();
        for(String s : inputStringArrayAfterSplit) {
            if(!Objects.equals(s, "")) {
                if(resultMap.containsKey(s)) {
                    resultMap.put(s, resultMap.get(s) + 1);
                } else {
                    resultMap.put(s, 1);
                }
            }
        }
        // Результат
        for(String key : resultMap.keySet()) {
            System.out.println( key + " - " + resultMap.get(key));
        }
    }
}
