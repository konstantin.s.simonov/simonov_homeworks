package com.company;

/**
 * Сделать класс Figure, у данного класса есть два поля - x и y координаты.
 *
 * Классы Ellipse и Rectangle должны быть потомками класса Figure.
 *
 * Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
 *
 * В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.
 */
public class Homework09 {

    public static void main(String[] args) {
        Figure figure = new Figure(2, 2);
        Figure ellipse = new Ellipse( 2, 2);
        Figure rectangle = new Rectangle( 2, 2);
        Figure square = new Square(2);
        Figure circle =new Circle( 2);

        System.out.println("figure perimeter: " + figure.getPerimeter());
        System.out.println("ellipse perimeter: " + ellipse.getPerimeter());
        System.out.println("rectangle perimeter: " + rectangle.getPerimeter());
        System.out.println("square perimeter: " + square.getPerimeter());
        System.out.println("circle perimeter: " + circle.getPerimeter());

    }
}
