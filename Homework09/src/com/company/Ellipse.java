package com.company;
public class Ellipse extends Figure {
        public Ellipse(double a, double b) {
            super( a, b);
        }
        /**
         *
         * @return P= 2π√(a2+b2)/2
         */
        public double getPerimeter() {

            return 2 * Math.PI * Math.sqrt((x * 2 + y * 2) / 2);
    }
}
