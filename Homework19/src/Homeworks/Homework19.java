package Homeworks;
import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public class Homework19 {
    public static void main(String[] args) {
// ? почему надо указать полный путь к users.txt если он находится в пакете???
        UsersRepository usersRepository = new UsersRepositoryFileImpl("C:\\Users\\konst\\Desktop\\simonov_homeworks\\Homework19\\src\\Homeworks\\users.txt");
        List<User> users = usersRepository.findAll();
// читаем из файла
        for (User user : users) {
            System.out.println("findAll: " + user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
// записываем в файл
        User user = new User("Игорь", 33, true);
        usersRepository.save(user);
// проверяем реализацию findByAge
        System.out.println();
        List<User> users1 = usersRepository.findByAge(25);
        for (User user1 : users1) {
            System.out.println("findByAge: " + user1.getAge() + " " + user1.getName() + " " + user1.isWorker());
        }
// проверяем реализацию findByIsWorkerIsTrue
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        System.out.println();
        for (User user1 : users2) {
            System.out.println("findByIsWorkerIsTrue: " + user1.getAge() + " " + user1.getName() + " " + user1.isWorker());
        }
    }
}

