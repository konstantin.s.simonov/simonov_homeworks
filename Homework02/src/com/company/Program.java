package com.company;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Program
{
    public static void main(String[] args)
    {
        System.out.println("Homework02");
        int[] intArray = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        // Вызов функции
        System.out.println(findIndex(intArray, 50));
        // Вызов процедуры
        orderArray(intArray);
        System.out.println(Arrays.toString(intArray));
    }

    /**
     * Процедура перемещение в массиве нулей в правую часть массива
     * @param array исходный массив преобразуется в процессе выполнения процедуры
     */
    public static void orderArray (int[] array)
    {
        int[] result = new int[array.length];
        int j = -1;
        for (int k : array)
        {
            if (k != 0)
            {
                j++;
                result[j] = k;
            }
        }
        //array = result;
        System.arraycopy(result, 0, array, 0, array.length);
    }
    /**
     * Функция поиск числа в массиве и выдача его индекса искомого числа
     * @param array исходный массив целочисленных значений
     * @param number искомое число
     * @return возвращает индекс искомого числа или возвращает -1 если число не будет найдено
     */
    public static int findIndex (int[] array, int number)
    {
        int index = -1;

        for (int i = 0; i < array.length; i++)
        {
            if(array[i] == number )
            {
                index = i;
                break;
            }
        }
        return index;
    }
}
