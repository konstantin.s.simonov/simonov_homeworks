-- создание таблицы - товар
create table product
(
    id          serial primary key, -- первичный ключ
    description varchar(20),        -- описание товара
    cost        decimal(5, 2),      -- стоимость
    quantity    decimal(5, 2)       -- количество
);

-- ввод данных в таблицу - товар
insert into product(description, cost, quantity)
values ('описание товар1', 1.1, 1.1);
insert into product(description, cost, quantity)
values ('описание товар2', 2.2, 2.2);
insert into product(description, cost, quantity)
values ('описание товар3', 3.3, 3.3);
insert into product(description, cost, quantity)
values ('описание товар4', 4.4, 4.4);
insert into product(description, cost, quantity)
values ('описание товар5', 5.5, 5.5);
insert into product(description, cost, quantity)
values ('описание товар6', 6.6, 6.6);
insert into product(description, cost, quantity)
values ('описание товар7', 7.7, 7.7);
insert into product(description, cost, quantity)
values ('описание товар8', 8.8, 8.8);
insert into product(description, cost, quantity)
values ('описание товар9', 9.9, 9.9);
insert into product(description, cost, quantity)
values ('описание товар10', 10.10, 10.10);

-- создание таблицы - заказчик
create table customer
(
    id         serial primary key, -- первичный ключ
    first_name varchar(20),        -- имя
    last_name  varchar(20)         -- фамилия
);

-- ввод данных в таблицу - заказчик
insert into customer(first_name, last_name)
values ('Имя1', 'Фамилия1');
insert into customer(first_name, last_name)
values ('Имя2', 'Фамилия2');
insert into customer(first_name, last_name)
values ('Имя3', 'Фамилия3');

-- создание таблицы - заказ
create table orders
(
    id            serial primary key,
    product_id    int,
    foreign key (product_id) references product (id),
    customer_id   int,
    foreign key (customer_id) references customer (id),
    data_order    date,
    count_product decimal(5, 2)
);

-- ввод данных в таблицу - заказ
insert into orders(product_id, customer_id, data_order, count_product)
values (1, 1, '01-01-21', 1.1);
insert into orders(product_id, customer_id, data_order, count_product)
values (2, 2, '02-02-21', 2.2);
insert into orders(product_id, customer_id, data_order, count_product)
values (3, 3, '03-03-21', 3.3);
insert into orders(product_id, customer_id, data_order, count_product)
values (4, 1, '04-04-21', 1.4);
insert into orders(product_id, customer_id, data_order, count_product)
values (5, 2, '05-05-21', 5.5);
insert into orders(product_id, customer_id, data_order, count_product)
values (6, 3, '06-06-21', 6.6);
insert into orders(product_id, customer_id, data_order, count_product)
values (7, 1, '07-07-21', 7.7);
insert into orders(product_id, customer_id, data_order, count_product)
values (8, 2, '08-08-21', 8.8);
insert into orders(product_id, customer_id, data_order, count_product)
values (9, 3, '09-09-21', 9.9);
insert into orders(product_id, customer_id, data_order, count_product)
values (10, 1, '10-10-21', 10.10);

-- запросы
-- 1) получить фамилия, имя заказчика, количество товара, дату заказа в заказах товара с id=3
select product_id, last_name, first_name, data_order, count_product
from orders
         inner join customer c on orders.customer_id = c.id
where product_id = 3;

-- 2) дабавляем заказ заказчика c id=1 на товар с id=3
insert into orders(product_id, customer_id, data_order, count_product)
values (3, 2, '21-05-21', 1.3);

-- 3) получаем все заказы за период с 01-05-2021 по 01-10-2021
select *
from orders
where data_order between '01-05-21' and '01-10-21';

-- 4) получить первые 5 товаров отсортированные по количеству товара в заказах,
-- наименование товара, сумму количества товара в заказах
select p.description,
       sum(count_product) as summ
from orders
         inner join product p on product_id = p.id
group by product_id, p.description
order by summ desc limit 5;

