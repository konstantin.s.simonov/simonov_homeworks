package Homeworks;


public class Logger {
    // 1.
    private static final Logger logger;
    // 2.
    static {
        logger = new Logger();
    }
    // 3.
    public static Logger getInstance() {
        return logger;
    }

    public static void log(String message) {
        System.out.println(message);
    }
}