package Homeworks;
/**
 * Homework - 11
 *
 * Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
 * Применить паттерн Singleton для Logger.
 */
public class Homework11 {

    public static void main(String[] args) {
        Logger logger1 = Logger.getInstance();
        logger1.log("Message logger1");

    }
}
