### Домашнее задание Тема 16

### Реализовать 
*removeAt(int index)* для *ArrayList*

Удаление элемента по индексу:

* исходный список - 45, 78, 10, 17, 89, 16, size = 6 

* метод removeAt(3)

* результат - 45, 78, 10, 89, 16, size = 5