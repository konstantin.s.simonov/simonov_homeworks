package Homework16;

/**
 * Домашнее задание Тема 16
 * Реализовать
 * removeAt(int index) для ArrayList
 *
 * Удаление элемента по индексу:
 * исходный список - 45, 78, 10, 17, 89, 16, size = 6
 * метод removeAt(3)
 * результат - 45, 78, 10, 89, 16, size = 5
 */
public class Homework16 {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);
        // содержимое исходного списка
        for(int i = 0; i < numbers.getLength(); i++) {
            System.out.print(numbers.get(i) + " ");
        }

        // удаляем элемент списка с индексом 3
        numbers.removeAt(3);

        // содержимое списка после удаления элемента
        System.out.println();
        for(int i = 0; i < numbers.getLength(); i++) {
            System.out.print(numbers.get(i) + " ");
        }
    }
}
