package Users;

import java.util.List;

public interface UsersRepository {

    List<User> findAll();

    User findById(int id);

    void update(User user);

    void save(User user, boolean appendToEndOfFile);

}