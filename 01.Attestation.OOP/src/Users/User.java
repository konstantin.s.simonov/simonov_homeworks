package Users;

import java.util.Objects;

public class User {
    private final int userId;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int userId, String name, int age, boolean isWorker) {
        this.userId = userId;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId && age == user.age && isWorker == user.isWorker && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, name, age, isWorker);
    }

    public String toString() {
        return "{ " +
                this.getUserId() + "|" +
                this.getAge() + "|" +
                this.getName() + "|" +
                this.isWorker() +
                " }";
    }
}