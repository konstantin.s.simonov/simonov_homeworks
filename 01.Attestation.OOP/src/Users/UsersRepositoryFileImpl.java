package Users;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Загрузка содержимого файла users.txt в список User
     *
     * @return - данные из файла users.txt
     * @author simonov konstantin
     * @data 26/11/2021
     */
    @Override
    public List<User> findAll() {
        List<User> result;
        boolean finished = false;
        List<User> users = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");

                if (parts.length != 4) {
                    finished = true;
                    break;
                }
                int userId = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                User newUser = new User(userId, name, age, isWorker);

                users.add(newUser);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("\n *** Файл " + fileName + " не найден! ***");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!finished) {
            result = users;
        } else {
            throw new IllegalArgumentException("\n *** Файл " + fileName + " не соответствует формату User! ***");
        }
        return result;
    }

    /**
     * Поиск записей User в списке по данным файла user.txt по индексу
     *
     * @param id - индекс записи
     * @return - в случае успеха поиска - набор данных соответствующих id, в случае неуспеха - null
     * @author simonov konstantin
     * @data 26/11/2021
     */
    public User findById(int id) {
        User result = null;
        List<User> users = this.findAll();

        if (users.size() != 0) {
            for (User userIterator : users) {
                if (userIterator.getUserId() == id) {
                    result = userIterator;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Обновление данных в файле users.txt по значениям User
     *
     * @param user - набор данных для записи в файл
     * @author simonov konstantin
     * @data 26/11/2021
     */
    public void update(User user) {
        List<User> users = findAll();

        for (User userIterator : users) {
            if (userIterator.getUserId() == user.getUserId()) {
                userIterator.setName(user.getName());
                userIterator.setAge(user.getAge());
                userIterator.setWorker(user.isWorker());
            }
        }
        int counter = 0;
        for (User userIterator : users) {
            save(userIterator, counter != 0);
            counter++;
        }
    }

    /**
     * Служебный метод для метода update
     * Добавляет в файл users.txt запись User
     *
     * @param user              - добавляемый набор данных
     * @param appendToEndOfFile - служебный параметр, true - добавляет данные в конец файла
     * @author simonov konstantin
     * @data 26/11/2021
     */
    @Override
    public void save(User user, boolean appendToEndOfFile) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            if (appendToEndOfFile) {
                writer = new FileWriter(fileName, true);
            } else {
                writer = new FileWriter(fileName, false);
            }

            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getUserId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}

