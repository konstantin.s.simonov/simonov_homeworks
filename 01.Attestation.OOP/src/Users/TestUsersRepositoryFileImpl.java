package Users;

import java.util.ArrayList;
import java.util.List;

public class TestUsersRepositoryFileImpl {
    // файл тестовых данных
    static String usersRepositoryFileName = "src/Attestation/userstest.txt";
    // тестируемый набор значений User
    static int testedUserId = 2;
    static String testedUserName = "Марсель";
    static int testedUserAge = 27;
    static boolean testedUserIsWorker = true;

    public static void main(String[] args) {
        testFindAll();
        testFindById();
        testUpdate();
        testSave();
    }

    /**
     * Тест для метода findAll
     */
    public static void testFindAll() {
        UsersRepository usersRepository = new UsersRepositoryFileImpl(usersRepositoryFileName);

        List<User> usersExpectedList = new ArrayList<>();
        User user = new User(0, "User0", 30, true);
        usersExpectedList.add(user);
        user = new User(1, "User1", 31, false);
        usersExpectedList.add(user);
        user = new User(2, "Марсель", 27, true);
        usersExpectedList.add(user);
        user = new User(3, "User3", 33, false);
        usersExpectedList.add(user);
        user = new User(4, "User4", 34, true);
        usersExpectedList.add(user);
        user = new User(5, "User5", 35, false);
        usersExpectedList.add(user);

        List<User> usersReceivedList = new UsersRepositoryFileImpl(usersRepositoryFileName).findAll();

        if (usersReceivedList.equals(usersExpectedList)) {
            System.out.println("testFindAll: Тест выполнен УСПЕШНО!");
        } else {
            System.out.println("testFindAll: Тест НЕ ВЫПОЛНЕН!");
        }
    }

    /**
     * Тест для метода findById
     */
    public static void testFindById() {
        UsersRepository usersRepository = new UsersRepositoryFileImpl(usersRepositoryFileName);

        User userExpected = new User(testedUserId, testedUserName, testedUserAge, testedUserIsWorker);

        User userReceived = new UsersRepositoryFileImpl(usersRepositoryFileName).findById(testedUserId);

        if (userReceived.equals(userExpected)) {
            System.out.println("testFindById: Тест выполнен УСПЕШНО!");
        } else {
            System.out.println("testFindById: Тест НЕ ВЫПОЛНЕН!");
        }
    }

    /**
     * Тест для метода save
     */
    public static void testSave() {
        UsersRepository usersRepository = new UsersRepositoryFileImpl(usersRepositoryFileName);

        User userExpected = new User(100, "User100", 100, false);
        usersRepository.save(userExpected, true);
        User userReceived = new UsersRepositoryFileImpl(usersRepositoryFileName).findById(100);

        if (userReceived.equals(userExpected)) {
            System.out.println("testSave: Тест выполнен УСПЕШНО!");
        } else {
            System.out.println("testSave: Тест НЕ ВЫПОЛНЕН!");
        }
        // Возвращаем содержимое userstest.txt в исходное состояние
        User user = new User(testedUserId, testedUserName, testedUserAge, testedUserIsWorker);
        usersRepository.update(user);
    }

    /**
     * Тест для метода update
     */
    public static void testUpdate() {
        UsersRepository usersRepository = new UsersRepositoryFileImpl(usersRepositoryFileName);

        User userExpected = new User(testedUserId, "User100", 100, false);

        usersRepository.update(userExpected);
        User userReceived = new UsersRepositoryFileImpl(usersRepositoryFileName).findById(testedUserId);

        if (userReceived.equals(userExpected)) {
            System.out.println("testUpdate: Тест выполнен УСПЕШНО!");
        } else {
            System.out.println("testUpdate: Тест НЕ ВЫПОЛНЕН!");
        }
        // Возвращаем содержимое userstest.txt в исходное состояние
        User user = new User(testedUserId, testedUserName, testedUserAge, testedUserIsWorker);
        usersRepository.update(user);
    }
}
