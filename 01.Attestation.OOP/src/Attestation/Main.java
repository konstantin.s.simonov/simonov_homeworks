package Attestation;

import Users.User;
import Users.UsersRepository;
import Users.UsersRepositoryFileImpl;

import java.util.List;

/*
    Читаем текстовый файл user.txt, содержащий список user-ов.
    Получаем запись user-а соответствующую заданному id.
    Изменяем значения найденной записи.
    Перезаписываем соответствующую запись в файле новыми значениями.
    Выводим содержимое измененного файла на экран.
 */
public class Main {

    public static void main(String[] args) {

        String usersRepositoryFileName = "src/Attestation/users.txt";
        UsersRepository usersRepository = new UsersRepositoryFileImpl(usersRepositoryFileName);

        List<User> users;

        int idForFind = 2;

        User user = usersRepository.findById(idForFind);
        if (user != null) {

            user.setName("Марсель");
            user.setAge(27);
            usersRepository.update(user);

            users = usersRepository.findAll();
            System.out.println("Содержимое файла репозитория после обновления:");
            for (User userIterator : users) {
                System.out.println(userIterator);
            }

        } else {
            System.out.println("User с id = " + idForFind + " в файле репозитория не найден.");
        }
    }
}