package Homework16_2;

/**
 * Тема 16. Связные списки и списки на основе массива
 * LinkedList
 * 12.11.2021
 * Симонов Константин
 * Реализовать метод T get(int index) для LinkedList
 */
public class Homework16_2 {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();

        list.add(34);
        System.out.println(list.get(0));
        list.add(-10);
        System.out.println(list.get(1));
        list.add(77);
        System.out.println(list.get(2));

        System.out.println(list.get(2));


    }
}
