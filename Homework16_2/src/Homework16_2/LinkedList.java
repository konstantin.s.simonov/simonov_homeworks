package Homework16_2;
/**
 * 11.11.2021
 * 17. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public class LinkedList<T> {
    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    /**
     * 12.11.2021
     * Симонов Константин
     * @param index - index элемента в списке
     * @return элемент списка
     */
    public T get(int index) {
        Node<T> current = first;
        if (first == null) {
            return null;
        }
        int i = 0;
        while (i != index && current != null) {
            current = current.next;
            i++;
        }
        return (T) current;
    }
}
