package Homeworks;

import java.util.Random;
import java.util.Scanner;

public class Homework21 {

    public static void main(String[] args) {
        Homework21 hw = new Homework21();

        int[] arrayOfNumbers;

        Scanner scannerConsole = new Scanner(System.in);

        System.out.print("Введите количество чисел:");
        int numbersCount = hw.getPositiveIntFromConsole(scannerConsole);

        System.out.print("Введите количество threads:");
        int threadsCount = hw.getPositiveIntFromConsole(scannerConsole);

        arrayOfNumbers = new int[numbersCount];
        hw.arrayRandomNumberInitialisation(arrayOfNumbers, 100);

        int realSum = 0;
        for (int arrayOfNumber : arrayOfNumbers) {
            realSum += arrayOfNumber;
        }

        System.out.println("Сумма без threads = " + realSum);

        int byThreadSum = 0;
        SumThread[] sumThread = hw.sumThreadsStart(arrayOfNumbers, threadsCount);
        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sumThread[i].getSum();
        }

        System.out.println("Сумма с threads = " + byThreadSum);
        System.out.println("Результат сравнения двух сумм = " + (realSum == byThreadSum));
    }

    /**
     * Заполняем исходный массив случайными числами в определенном диапазоне
     *
     * @param array заполняемый массив чисел
     * @param bound диапазон случайных чисел
     */
    public void arrayRandomNumberInitialisation(int[] array, int bound) {

        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(bound);
        }
    }

    /**
     * Получаем из консоли целое положительное число
     *
     * @param console источник (System.in)
     * @return полученное целое положительное число
     * <p>
     * Числа меньше 1 не принимаются!
     * Не числа тоже не принимаются!
     */
    public int getPositiveIntFromConsole(Scanner console) {
        int number;
        do {
            System.out.println(" (введите положительное число!)");
            while (!console.hasNextInt()) {
                System.out.println(" Это не число!");
                console.next();
            }
            number = console.nextInt();
        } while (number <= 0);
        return number;
    }

    /**
     * Создаются threads.
     * Для каждого thread определяется диапазон элементов исходного массива.
     * Запускаются threads.
     *
     * @param arrayOfNumbers исходный массив
     * @param threadsCount   количество threads
     * @return массив threads
     */
    public SumThread[] sumThreadsStart(int[] arrayOfNumbers, int threadsCount) {
        int numbersCount = arrayOfNumbers.length;
        SumThread[] sumThread = new SumThread[threadsCount];
        int intervalOfIndex = numbersCount / threadsCount;
        int fromIndex;
        int toIndex;
        for (int i = 0; i < threadsCount; i++) {
            fromIndex = i * intervalOfIndex;
            toIndex = fromIndex + intervalOfIndex;

            if ((i + 1) == threadsCount) {
                toIndex = numbersCount;
            }
            sumThread[i] = new SumThread(i, arrayOfNumbers, fromIndex, toIndex);
        }
        try {
            for (int i = 0; i < threadsCount; i++) {
                sumThread[i].start();
                sumThread[i].join(); // Обеспечивает последовательное суммирование по threads
            }
        } catch (InterruptedException e) {
            return null;
        }
        return sumThread;
    }
}
