package Homeworks;

public class SumThread extends Thread {
    private final int threadIndex;
    private final int[] array;
    private final int from;
    private final int to;
    private int sum;

    public SumThread(int threadIndex, int[] array, int from, int to) {
        this.threadIndex = threadIndex;
        this.array = array;
        this.from = from;
        this.to = to;
    }

    @Override
    /*
      При выполнении tread выполняется суммирование распределенного для данного thread элементов исходного массива
     */
    public void run() {
        for (int i = from; i < to; i++) {
            sum += array[i];
        }
    }

    public int getSum() {
        return this.sum;
    }
}
