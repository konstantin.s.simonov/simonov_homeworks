package Homeworks;

public class TestHomeworks21 {

    private final Homework21 hw = new Homework21();

    public static void main(String[] args) {
        TestHomeworks21 testHw = new TestHomeworks21();

        testHw.testArrayRandomNumberInitialisation();
        testHw.testGetPositiveIntFromConsole();
        testHw.testSumThreadsStart();
    }

    /**
     * Тест метода arrayRandomNumberInitialisation
     * проверяется генерация случайных чисел в диапазоне bound
     */
    public void testArrayRandomNumberInitialisation() {
        int[] array = new int[10];
        hw.arrayRandomNumberInitialisation(array, 100);
        for (int i : array) {
            if (i > 100) {
                System.out.println("Тест НЕ выполнен !");
                return;
            }
        }
        System.out.println("Тест выполнен УСПЕШНО!");
    }

    /**
     * Тест метода testGetPositiveIntFromConsole()
     */
    public void testGetPositiveIntFromConsole() {
        System.out.println("Тест в разработке");
    }

    /**
     * Тест метода testSumThreadsStart
     * <p>
     * Сравнение с просчитанным результатом
     */
    public void testSumThreadsStart() {
        // тестовые данные
        int[] array = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int expectedSum = 10;

        SumThread[] threads = new SumThread[2];
        threads = hw.sumThreadsStart(array, 2);
        if (expectedSum != (threads[0].getSum() + threads[1].getSum())) {
            System.out.println("Тест НЕ выполнен !");
            return;
        }
        System.out.println("Тест выполнен УСПЕШНО!");
    }
}
