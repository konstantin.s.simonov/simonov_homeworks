package com.company;

public class Human {
    private String name;
    private double weight;

    /**
     * сеттер name
     * @param name
     */
    void setName (String name) {
        this.name = name;
    }

    /**
     * сеттер weight
     * @param weight
     */
    void setWeight (double weight) {
        this.weight = weight;
    }

    /**
     * геттер name
     * @return name
     */
    String getName() {
        return this.name;
    }

    /**
     * геттер - weight
     * @return weight
     */
    double getWeight() {
        return this.weight;
    }
}
