/**
 * Homework - 08
 * На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
 * Считать эти данные в массив объектов.
 * Вывести в отсортированном по возрастанию веса порядке.
 */
package com.company;
//import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human[] arrayHumans = new Human[10];
        for (int i = 0; i < arrayHumans.length; i++) {
            arrayHumans[i] = new Human();
        }
// ввод имени
        System.out.println("Ввод имён ... ");
        int i = 0;
        for (Human h : arrayHumans) {
            System.out.print("Введите ИМЯ № " + i + " - ");
            String name = scanner.nextLine();
            h.setName(name);
            i++;
        }
// ввод веса
        System.out.println("Ввод веса ... ");
        i = 0;
        for (Human h : arrayHumans) {
            System.out.print("Введите ВЕС № " + i + " - ");
            double weight = scanner.nextDouble();
            h.setWeight(weight);
            i++;
        }
        // сортировка массива по возрастанию веса
        System.out.println("Сортировка ... ");
        bubbleSort(arrayHumans);
        // вывод данных отсортированного массива
        System.out.println("Вывод отсортированного массива...");
        for (Human h : arrayHumans) {
            System.out.println("ИМЯ: " + h.getName() + " ВЕС: " + h.getWeight());
        }
    }
    /**
     * Пузырьковая сортировка массива по возрастанию веса
     * @param arr - сортируемый массив
     */
        public static void bubbleSort(Human[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j].getWeight() > arr[j + 1].getWeight()) {
                    Human tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }
}