package ru.pcs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2",
            "postgres", "3005");
        ProductsRepository productsRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);

        // получение списка всех товаров
        System.out.println("\nПолучение списка всех товаров:");
        System.out.println(productsRepository.findAll());

        // получение списка товаров по цене заданной цене
        System.out.println("\nПолучение списка товаров по заданной цене:");
        double price = 7.7;
        System.out.println("Цена = " + price);
        System.out.println(productsRepository.findAllByPrice(price));

        // получение списка товаров по количеству заказов, в которых они участвуют
        System.out.println("\nПолучение списка товаров по количеству заказов, в которых они участвуют");
        int ordersCount = 2;
        System.out.println("Количество = " + ordersCount);
        System.out.println(productsRepository.findAllByOrdersCount(ordersCount));
    }
}
