package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcTemplateImpl implements ProductsRepository {


    private final JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> ProductRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        float cost = row.getFloat("cost");
        float quantity = row.getFloat("quantity");

        return new Product(id, description, cost, quantity);
    };

    /**
     * Получение списка всех товаров
     *
     * @return -  список Product по результатам выборки. null если не найден
     */
    @Override
    public List<Product> findAll() {
        List<Product> list;
        //language=SQL
        String SQL_SELECT_ALL = "select * from product order by id";

        list = jdbcTemplate.query(SQL_SELECT_ALL, ProductRowMapper);
        if (list.isEmpty()) {
            return null;
        } else {
            return list;
        }
    }

    /**
     * Получение списка товаров по заданной цене
     *
     * @param price - цена
     * @return - список Product по результатам выборки. null если не найден
     */
    @Override
    public List<Product> findAllByPrice(double price) {
        List<Product> list;
        //language=SQL
        String SQL_SELECT_ALL_BY_PRICE = "select * from product where cost=" + price + " order by id";
        list = jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, ProductRowMapper);
        if (list.isEmpty()) {
            return null;
        } else {
            return list;
        }
    }

    /**
     * Получение списка товаров по количеству заказов, в которых они участвуют
     *
     * @param ordersCount - количество заказов
     * @return - список Product по результатам выборки. null если не найден
     */
    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        List<Product> list;
        //language=SQL
        String SQL_SELECT_ALL_BY_ORDERS_COUNT = "select id, description, cost, quantity " +
                "from ( " +
                "select product_id as id, " +
                "sub2.description, " +
                "sub2.cost, " +
                "sub2.quantity, " +
                "count(product_id) as i " +
                "from ( select * from orders ) as sub1 " +
                "inner join ( select * from product ) as sub2 " +
                "on product_id = sub2.id " +
                "group by product_id, sub2.description, sub2.cost, sub2.quantity) " +
                "as itog where i = " + ordersCount;

        list = jdbcTemplate.query(SQL_SELECT_ALL_BY_ORDERS_COUNT, ProductRowMapper);
        if (list.isEmpty()) {
            return null;
        } else {
            return list;
        }
    }
}
