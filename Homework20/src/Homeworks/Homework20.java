package Homeworks;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.stream.Collectors;


import static java.lang.Integer.*;

/**
 * 18/11/2021
 * Homeworks - 20
 *
 * @author simonov konstantin
 * @version v2.0
 *
 * o001aa111|Camry|Black|133000|720000
 * o002aa111|Camry|Green|0|1550000
 * o003aa111|Camry|Black|13003|820000
 * o001aa111|Camry|Black|133000|720000
 * o010aa111|Camry|Red|133000|700000
 * o101aa111|Toyota|Blue|50000|900000
 *
 */
public class Homework20 {
    public static void main(String[] args) {

        // 1. Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
        try (BufferedReader reader = new BufferedReader(new FileReader("src//Homeworks//cars.txt"))) {
            System.out.println(" 1. Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
            reader.lines()
                    .map(element -> element.split("\\|"))
                    .filter(element -> (element[2].equals("Black") || parseInt(element[3]) == 0))
                    .forEach(element -> System.out.println(
                            "Номерной знак: " + element[0])
                    );
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
// 2. Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        try (BufferedReader reader = new BufferedReader(new FileReader("src//Homeworks//cars.txt"))) {
            System.out.println("\n 2. Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
            System.out.println("Количество: " +
                    reader.lines()
                            .distinct()
                            .map(element -> element.split("\\|"))
                            .filter(element -> (parseInt(element[4]) >= 700000 && parseInt(element[4]) <= 800000)).count()
            );
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

// 3. Вывести цвет автомобиля с минимальной стоимостью.
        // 3.1. Находим минимальную стоимость
        int minPrice;
        try(
                BufferedReader reader = new BufferedReader(new FileReader("src//Homeworks//cars.txt")))
        {
            System.out.println("\n 3. Цвет автомобиля с минимальной стоимостью.");
            System.out.print("Цвет: ");
            minPrice = reader.lines()
                    .map(element -> element.split("\\|"))
                    .map(element -> parseInt(element[4]))
                    .min(Comparator.comparing(Integer::valueOf))
                    .get();
        } catch(IOException e) {
            throw new IllegalArgumentException(e);
        }
        // 3.2. Выводим цвет автомобиля с минимальной стоимостью
        try(
                BufferedReader reader = new BufferedReader(new FileReader("src//Homeworks//cars.txt")))
        {
            reader.lines()
                    .distinct()
                    .map(element -> element.split("\\|"))
                    .filter(element -> (parseInt(element[4]) == minPrice))
                    .forEach(element -> System.out.println(element[2]));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
// 4. Вывести среднюю стоимость Camry.
        try(
                BufferedReader reader = new BufferedReader(new FileReader("src//Homeworks//cars.txt")))
        {
            System.out.println("\n 4. Средняя стоимость Camry.");
            System.out.print("Средняя стоимость: " +
                    reader.lines()
                            .distinct()
                            .map(element -> element.split("\\|"))
                            .filter(element -> (element[1].equals("Camry")))
                            .collect(Collectors.averagingInt(element -> Integer.parseInt(element[4]))));
        } catch(IOException e) {
            throw new IllegalArgumentException(e);
        }
        System.out.println();
    }
}

