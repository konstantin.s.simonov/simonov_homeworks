package com.company;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Тема 7. Алгоритмы и структуры данных, оценка сложности алгоритмов
 * Homework - 07
 * На вход подается последовательность чисел, оканчивающихся на -1.
 * Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
 * Гарантируется:
 * Все числа в диапазоне от -100 до 100.
 * Числа встречаются не более 2 147 483 647-раз каждое.
 * Сложность алгоритма - O(n)
 */
public class Main {
    public static void main(String[] args) {
        // write your code here
        int number;
        int min = Integer.MAX_VALUE;
        int index = -1;
        // объявляем массив целых чисел в диапазоне -100..100
        int[] array = new int[200];
        Scanner scanner = new Scanner(System.in);
        // пока на входе не появится -1
        do {
            number = scanner.nextInt();
            if(number == -1) {
                min = 0;
                break;
            }
            if (number >= -100 && number <= 100) {
                array[number+100] += 1;
            }
        } while (true);
        // числа в массиве имеют свой индекс -100:0, -1:99, 0:100, 100:200
        for (int i = 0; i < 200; i++) {
            if(array[i] != 0 && array[i] < min ) {
                min = array[i];
                index = i-100;
            }
        }
        System.out.println("Число " + index + " присутствует в последовательности " + min + " раз.");
    }
}
