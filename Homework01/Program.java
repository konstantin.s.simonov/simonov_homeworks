package com.company;
import java.util.Scanner;

public class Program
{

    public static void main(String[] args)
    {
        int number;
        int min = Integer.MAX_VALUE;
        int minDigitResult = min;
        Scanner scanner = new Scanner(System.in);
        do
        {
            number = scanner.nextInt();
            if (number == -1)
            {
                break;
            }
            min = minDigit(number);
            if (minDigitResult > min)
            {
                minDigitResult = min;
            }
        } while (true);
        System.out.println(minDigitResult);
    }

    public static int minDigit(int i)
    {
        int min = Integer.MAX_VALUE;
        while (i != 0)
        {
            int ii = i % 10;
            i /= 10;
            if( min > ii )
            {
                min = ii;
            }
        }
        return min;
    }
}
