package Homeworks;

public interface Movable
{
    void moveTo(double x, double y);
}
