package Homeworks;

public abstract class Figure
{
    protected double x;
    protected double y;
    protected double a;
    protected double b;

    public Figure(double x, double y, double a, double b)
    {
        this.x = x;
        this.y = y;
        this.a = a;
        this.b = b;
    }

    public double getPerimeter()
    {
        return 0;
    }
}