package Homeworks;
public class Circle extends Ellipse implements Movable
{
    public Circle(double x, double y, double r)
    {
        super(x, y, r, r);
    }

    @Override
    public void moveTo(double x, double y) {
        super.x = x;
        super.y = y;
    }
}