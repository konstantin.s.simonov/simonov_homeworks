package Homeworks;

/*
 * Homework 09
 * Сделать класс Figure, у данного класса есть два поля - x и y координаты.
 * Классы Ellipse и Rectangle должны быть потомками класса Figure.
 * Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
 * В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.
 */
/**
 * Homework 10
 * Сделать класс Figure из задания 09 абстрактным.
 * Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
 * Данный интерфейс должны реализовать только классы Circle и Square.
 * В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
 */
public class Homework10 {

    public static void main(String[] args)
    {
        Figure[] figures = new  Figure[2];
        // Создаем фигуры, размер фигуры и начальные координаты и размещаем в массиве
        figures[0] = new Circle(0,0, 2);
        figures[1] = new Square(1, 1, 2);

        System.out.println("Выводим значение длины периметра фигуры ...");
        System.out.println("square perimeter: " + figures[0].getPerimeter());
        System.out.println("circle perimeter: " + figures[1].getPerimeter());

        System.out.println("Выводим начальные координаты фигур...");
        System.out.println("square координаты: " + figures[0].x + ", " + figures[0].y );
        System.out.println("circle координаты: " + figures[1].x + ", " + figures[1].y );

        // Перемещение фигур в точку (10 , 10) применяя метод moveTo
        Movable[] movables = { (Movable) figures[0], (Movable) figures[1]};
        movables[0].moveTo(10,10);
        movables[1].moveTo(10,10);
        System.out.println("Выводим измененные координаты фигур...");
        System.out.println("square координаты: " + figures[0].x + ", " + figures[0].y );
        System.out.println("circle координаты: " + figures[1].x + ", " + figures[1].y );

    }
}

