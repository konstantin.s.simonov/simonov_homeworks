package Homeworks;

public class Rectangle extends Figure {
    public Rectangle (double x, double y, double a, double b)
    {
        super( x, y, a, b);
    }
    public double getPerimeter()
    {
        return a * 2 + b * 2;
    }
}