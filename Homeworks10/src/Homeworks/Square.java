package Homeworks;

public class Square extends Rectangle implements Movable {

    public Square(double x, double y, double length) {
        super(x, y, length, length);
    }

    @Override
    public void moveTo(double x, double y) {
        super.x = x;
        super.y = y;
    }
}