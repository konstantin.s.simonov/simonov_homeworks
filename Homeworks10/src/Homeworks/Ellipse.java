package Homeworks;
public class Ellipse extends Figure
{
    public Ellipse(double x, double y, double a, double b)
    {
        super( x, y, a, b);
    }
    /**
     *
     * @return P= 2π√(a2+b2)/2
     */
    public double getPerimeter()
    {
        return 2 * Math.PI * Math.sqrt((a * 2 + b * 2) / 2);
    }
}
