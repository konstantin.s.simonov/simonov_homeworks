package Homework13;

import java.util.Arrays;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int i = 0;

        int[] result = new int[array.length];

        for (int x : array) {
            if (condition.isOk(x)) {
                result[i++] = x;
            }
        }
        return Arrays.copyOfRange(result, 0, i);
    }
}
