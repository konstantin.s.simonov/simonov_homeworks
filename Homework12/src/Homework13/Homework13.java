package Homework13;

import java.util.Arrays;

/**
 * Homework - 13
 *
 * Предусмотреть функциональный интерфейс
 * interface ByCondition {
 * boolean isOk(int number);
 * }
 * Реализовать в классе Sequence метод:
 * public static int[] filter(int[] array, ByCondition condition) {
 * ...
 * }
 * Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
 * В main в качестве condition подставить:
 * 1) проверку на четность элемента
 * 2) проверку, является ли сумма цифр элемента четным числом.
 */
public class Homework13 {

    public static void main(String[] args) {
        int[] inputArray = {-11, 21, 22, 123, 45666, 51231, 6888, -527, 8};

        // 1) Проверка на четность элемента
        System.out.println("1. Проверка на четность элемента...");
        for (int i : Sequence.filter(inputArray, (number -> number % 2 == 0))) {
            System.out.print(i + " ");
        }
        // 2) Проверка является ли сумма цифр элемента четным числом
        System.out.println();
        System.out.println("2. Проверка является ли сумма цифр элемента четным числом...");
        for (int i : Sequence.filter(inputArray, (number -> sumDigitsNumber(number) % 2 == 0))) {
            System.out.print(i + " ");
        }
    }

    /**
     * sum of the digits of the number
     * @return - sum of the digits of the number
     */
    static int sumDigitsNumber(int n){
        int number = Math.abs(n);
        return number >= 10 ? number % 10 + sumDigitsNumber(number / 10) : number;
    }
}
